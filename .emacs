;; Tested on Mac OS X with Emacs 25.1, Windows 10 and Ubuntu Linux with Emacs 24.4
;; Requires the following packages:
;;
;; abyss-theme        20160420.512  installed             A dark theme with contrasting colours.
;; all-the-icons      20170509.318  installed             A library for inserting Developer icons
;; auctex             11.90.2       installed             Integrated environment for *TeX*
;; auctex-latexmk     20160923.7    installed             Add LatexMk support to AUCTeX
;; auto-indent-mode   20161118.1458 installed             Auto indent Minor mode
;; biblio             20161014.1604 installed             Browse and import bibliographic references from CrossRef, arXiv, DBLP, HAL, Dissemin, and doi.org
;; bibretrieve        20170417.620  installed             Retrieving BibTeX entries from the web
;; bibtex-utils       20170221.1757 installed             Provides utilities for extending BibTeX mode
;; bm                 20170103.1424 installed             Visible bookmarks in buffer.
;; comment-dwim-2     20150825.1549 installed             An all-in-one comment command to rule them all
;; company            20170420.1837 installed             Modular text completion framework
;; company-auctex     20161025.24   installed             Company-mode auto-completion for AUCTeX
;; company-bibtex     20170125.2135 installed             Company completion for bibtex keys
;; company-math       20170221.751  installed             Completion backends for unicode math symbols and latex tags
;; company-statistics 20170210.1133 installed             Sort candidates using completion history
;; counsel            20170506.1638 installed             Various completion functions using Ivy
;; ebib               20170401.1342 installed             a BibTeX database manager
;; embrace            20170508.1526 installed             Add/Change/Delete pairs based on `expand-region'
;; expand-region      20170222.343  installed             Increase selected region by semantic units.
;; fancy-narrow       20160124.403  installed             narrow-to-region with more eye candy.
;; golden-ratio-sc... 20170223.1829 installed             Scroll half screen down or up, and highlight current line
;; goto-last-change   20150109.1023 installed             Move point through buffer-undo-list positions
;; gscholar-bibtex    20170509.1134 installed             Retrieve BibTeX from Google Scholar and other online sources(ACM, IEEE, DBLP)
;; highlight-inden... 20170508.1133 installed             Minor mode to highlight indentation
;; highlight-paren... 20151107.2316 installed             highlight surrounding parentheses
;; hl-line+           20170223.745  installed             Extensions to hl-line.el.
;; hl-todo            20161102.1337 installed             highlight TODO and similar keywords
;; hlinum             20170507.2227 installed             Extension for linum.el to highlight current line number
;; ivy-bibtex         20170321.1306 installed             A bibliography manager based on Ivy
;; ivy-hydra          20170412.30   installed             Additional key bindings for Ivy
;; latex-pretty-sy... 20151112.244  installed             Display many latex symbols as their unicode counterparts
;; math-symbol-lists  20170221.553  installed             Lists of Unicode math symbols and latex commands
;; move-text          20170213.2128 installed             Move current line or region with M-up or M-down.
;; neotree            20170507.1711 installed             A tree plugin like NerdTree for Vim
;; org                20170502      installed             Outline-based notes management and organizer
;; org-bookmark-he... 20170510.1008 installed             Emacs bookmark support for org-mode
;; org-ref            20170509.1755 installed             citations, cross-references and bibliographies in org-mode
;; org-sticky-header  20170422.2135 installed             Show off-screen Org heading at top of window
;; smart-yank         0.1.1         installed             A different approach of yank pointer handling
;; smex               20151212.1409 installed             M-x interface with Ido-style fuzzy matching.
;; undo-tree          20161012.701  installed             Treat undo history as a tree
;; visual-fill-column 20170502.1300 installed             fill-column for visual-line-mode
;; visual-regexp      20170228.1716 installed             A regexp/replace command for Emacs with interactive visual feedback
;; visual-regexp-s... 20170221.1853 installed             Extends visual-regexp to support other regexp engines
;; volatile-highli... 20160611.1855 installed             Minor mode for visual feedback on some operations.
;; which-key          20170501.544  installed             Display available keybindings in popup
;; ws-butler          20170111.1534 installed             Unobtrusively remove trailing whitespace.
;; yasnippet          20170418.351  installed             Yet another snippet extension for Emacs.
;; async              20170502.2343 dependency            Asynchronous processing in Emacs
;; biblio-core        20160901.1115 dependency            A framework for looking up and displaying bibliographic entries
;; dash               20170207.2056 dependency            A modern list library for Emacs
;; epl                20150517.433  dependency            Emacs Package Library
;; f                  20170404.1039 dependency            Modern API for working with files and directories
;; flyspell-correct   20170213.700  dependency            correcting words with flyspell via custom interface
;; font-lock+         20170222.1755 dependency            Enhancements to standard library `font-lock.el'.
;; fullframe          20160504.749  dependency            Generalized automatic execution in a single frame
;; helm               20170510.853  dependency            Helm is an Emacs incremental and narrowing framework
;; helm-bibtex        20170321.1306 dependency            A bibliography manager based on Helm
;; helm-core          20170507.531  dependency            Development files for Helm
;; hydra              20170325.815  dependency            Make bindings that stick around.
;; ivy                20170501.1903 dependency            Incremental Vertical completYon
;; key-chord          20160227.438  dependency            map pairs of simultaneously pressed keys to commands
;; load-relative      20160716.438  dependency            relative file load (within a multi-file Emacs package)
;; loc-changes        20160801.1008 dependency            keep track of positions even after buffer changes
;; memoize            20130421.1234 dependency            Memoization functions
;; parsebib           20170501.347  dependency            A library for parsing bib files
;; pdf-tools          20170417.150  dependency            Support library for PDF documents.
;; pkg-info           20150517.443  dependency            Information about packages
;; popup              20160709.729  dependency            Visual Popup User Interface
;; pos-tip            20150318.813  dependency            Show tooltip at point
;; powerline          20161121.2320 dependency            Rewrite of Powerline
;; s                  20170428.1026 dependency            The long lost Emacs string manipulation library.
;; seq                2.20          dependency            Sequence manipulation functions
;; sr-speedbar        20161025.131  dependency            Same frame speedbar
;; swiper             20170410.24   dependency            Isearch with an overview. Oh, man!
;; tablist            20170219.1935 dependency            Extended tabulated-list-mode
;; test-simple        20170117.411  dependency            Simple Unit Test Framework for Emacs Lisp

;; Copyright (C) 2016-2017 Laurent Hoeltgen

;; This program is free software: you can redistribute it and/or modify it under
;; the terms of the GNU General Public License as published by the Free Software
;; Foundation, either version 3 of the License, or (at your option) any later
;; version.

;; This program is distributed in the hope that it will be useful, but WITHOUT
;; ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;; details.

;; You should have received a copy of the GNU General Public License along with
;; this program. If not, see <http://www.gnu.org/licenses/>.

;; https://github.com/nasseralkmim/.emacs.d/blob/master/config.org
;; UTF-8 please
(set-language-environment "UTF-8")
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))
(setq-default indent-tabs-mode nil)

(set-default-coding-systems 'utf-8)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(require 'cl)

;; Display current plattform. Useful for system specific settings.
(cond
 ((string-equal system-type "windows-nt") ; Microsoft Windows
  (progn
    (message "Microsoft Windows")
    ))
 ((string-equal system-type "darwin") ; Mac OS X
  (progn
    (message "Mac OS X")
    ;; (setq myHomeFolder "/Users/laurent/")
    (setenv "PATH" (concat "/Library/TeX/texbin" ":" (getenv "PATH")))
    (setq exec-path (append exec-path '("/Library/TeX/texbin")))
    (setenv "PATH" (concat "/Users/laurent/bin" ":" (getenv "PATH")))
    (setq exec-path (append exec-path '("/Users/laurent/bin")))
    ;; (setenv "PATH" (concat "/opt/local/bin/" ":" (getenv "PATH")))
    ;; (setq exec-path (append exec-path '("/opt/local/bin/")))
    (setenv "PATH" (concat "/usr/local/bin/" ":" (getenv "PATH")))
    (setq exec-path (append exec-path '("/usr/local/bin/")))
    ))
 ((string-equal system-type "gnu/linux") ; linux
  (progn
    (message "Linux")
    ))
 )
;; ------------------------------------------------------------------------------
(defun unfill-region (begin end)
  "Change isolated newlines in region into spaces."
  (interactive (if (use-region-p)
		   (list (region-beginning)
			 (region-end))
		 (list nil nil)))
  (save-restriction
    (narrow-to-region (or begin (point-min))
		      (or end (point-max)))
    (goto-char (point-min))
    (while (search-forward "\n" nil t)
      (if (eq (char-after) ?\n)
	  (skip-chars-forward "\n")
	(delete-char -1)
	(insert ?\s)))))
;; -----------------------------------------------------------------------------
(defun endless/fill-or-unfill ()
  "Like `fill-paragraph', but unfill if used twice."
  (interactive)
  (let ((fill-column
         (if (eq last-command 'endless/fill-or-unfill)
             (progn (setq this-command nil)
                    (point-max))
           fill-column)))
    (call-interactively #'fill-paragraph)))
;;
(global-set-key [remap fill-paragraph]
                #'endless/fill-or-unfill)
;; -----------------------------------------------------------------------------
(defun goto-line-with-feedback ()
  "Show line numbers temporarily, while prompting for the line number input"
  (interactive)
  (unwind-protect
      (progn
	(hlinum-activate)
        (linum-mode 1)
	(hl-line-mode 1)
        (goto-line (read-number "Goto line: ")))
    (hlinum-deactivate)
    (hl-line-mode -1)
    (linum-mode -1)))
;; -----------------------------------------------------------------------------
(defun goto-column (number)
  "Untabify, and go to a column NUMBER within the current line (0
is beginning of the line)."
  (interactive "nColumn number: ")
  (move-to-column number t))
;; -----------------------------------------------------------------------------
(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))
  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg)))
    )
  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))
;; -----------------------------------------------------------------------------
(add-hook 'isearch-mode-end-hook
          #'endless/goto-match-beginning)
(defun endless/goto-match-beginning ()
  "Go to the start of current isearch match.
Use in `isearch-mode-end-hook'."
  (when (and isearch-forward
             (number-or-marker-p isearch-other-end)
             (not mark-active)
             (not isearch-mode-end-hook-quit))
    (goto-char isearch-other-end)))
;; -----------------------------------------------------------------------------
;; Put the point immediately after a closing paren. replace-matching-parens will
;; replace the closing ) with \right) and the matching start paren ( with
;; \left(.
(defun replace-matching-parens (char)
  (interactive (list (char-before)))
  (unless (memq char '(?\) ?\] ?\}))
    (error "Cursor is not after `)', `]', or `}'"))
  (save-excursion
    (let ((syntable   (copy-syntax-table (syntax-table)))
          (end-point  (point)))
      (modify-syntax-entry ?\[ "(" syntable)
      (modify-syntax-entry ?\] ")" syntable)
      (modify-syntax-entry ?\{ "(" syntable)
      (modify-syntax-entry ?\} ")" syntable)
      (with-syntax-table syntable
        (backward-list)
        (let ((start-point  (point)))
          (goto-char end-point)
          (search-backward (format "%c" char) nil t)
          (replace-match (format " \\\\right%c" char) nil nil)
          (goto-char start-point)
          (search-forward (format "%c" (setq newchar  (case char
                                                        (?\) ?\( )
                                                        (?\] ?\[ )
                                                        (?\} ?\{ ))))
                          nil t)
          (replace-match (format "\\\\left%c " newchar) nil nil))))))
;; -----------------------------------------------------------------------------
(defun annotate-todo ()
  "put fringe marker on TODO lines in the curent buffer"
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (while (re-search-forward "TODO" nil t)
      (let ((overlay (make-overlay (- (point) 5) (point))))
        (overlay-put overlay 'before-string
                     (propertize "A"
                                 'display '(right-fringe left-triangle)))))))
;; -----------------------------------------------------------------------------
;; latex mode
;; http://emacs.stackexchange.com/questions/3083/how-to-indent-items-in-latex-auctex-itemize-environments
(defun LaTeX-indent-item ()
  "Provide proper indentation for LaTeX \"itemize\",\"enumerate\", and
\"description\" environments.

  \"\\item\" is indented `LaTeX-indent-level' spaces relative to
  the the beginning of the environment.

  Continuation lines are indented either twice
  `LaTeX-indent-level', or `LaTeX-indent-level-item-continuation'
  if the latter is bound."
  (save-match-data
    (let* ((offset LaTeX-indent-level)
           (contin (or (and (boundp 'LaTeX-indent-level-item-continuation)
                            LaTeX-indent-level-item-continuation)
                       (* 2 LaTeX-indent-level)))
           (re-beg "\\\\begin{")
           (re-end "\\\\end{")
           (re-env "\\(itemize\\|\\enumerate\\|description\\)")
           (indent (save-excursion
                     (when (looking-at (concat re-beg re-env "}"))
                       (end-of-line))
                     (LaTeX-find-matching-begin)
                     (current-column))))
      (cond ((looking-at (concat re-beg re-env "}"))
             (or (save-excursion
                   (beginning-of-line)
                   (ignore-errors
                     (LaTeX-find-matching-begin)
                     (+ (current-column)
                        (if (looking-at (concat re-beg re-env "}"))
                            contin
                          offset))))
                 indent))
            ((looking-at (concat re-end re-env "}"))
             indent)
            ((looking-at "\\\\item")
             (+ offset indent))
            (t
             (+ contin indent))))))

(defcustom LaTeX-indent-level-item-continuation 4
  "*Indentation of continuation lines for items in itemize-like
environments."
  :group 'LaTeX-indentation
  :type 'integer)
;; -----------------------------------------------------------------------------
;; https://fuco1.github.io/2017-05-06-Enhanced-beginning--and-end-of-buffer-in-special-mode-buffers-(dired-etc.).html
(defmacro my-special-beginning-of-buffer (mode &rest forms)
  "Define a special version of `beginning-of-buffer' in MODE.

The special function is defined such that the point first moves
to `point-min' and then FORMS are evaluated.  If the point did
not change because of the evaluation of FORMS, jump
unconditionally to `point-min'.  This way repeated invocations
toggle between real beginning and logical beginning of the
buffer."
  (declare (indent 1))
  (let ((fname (intern (concat "my-" (symbol-name mode) "-beginning-of-buffer")))
        (mode-map (intern (concat (symbol-name mode) "-mode-map")))
        (mode-hook (intern (concat (symbol-name mode) "-mode-hook"))))
    `(progn
       (defun ,fname ()
         (interactive)
         (let ((p (point)))
           (goto-char (point-min))
           ,@forms
           (when (= p (point))
             (goto-char (point-min)))))
       (add-hook ',mode-hook
                 (lambda ()
                   (define-key ,mode-map
                     [remap beginning-of-buffer] ',fname))))))
;;
(defmacro my-special-end-of-buffer (mode &rest forms)
  "Define a special version of `end-of-buffer' in MODE.

The special function is defined such that the point first moves
to `point-max' and then FORMS are evaluated.  If the point did
not change because of the evaluation of FORMS, jump
unconditionally to `point-max'.  This way repeated invocations
toggle between real end and logical end of the buffer."
  (declare (indent 1))
  (let ((fname (intern (concat "my-" (symbol-name mode) "-end-of-buffer")))
        (mode-map (intern (concat (symbol-name mode) "-mode-map")))
        (mode-hook (intern (concat (symbol-name mode) "-mode-hook"))))
    `(progn
       (defun ,fname ()
         (interactive)
         (let ((p (point)))
           (goto-char (point-max))
           ,@forms
           (when (= p (point))
             (goto-char (point-max)))))
       (add-hook ',mode-hook
                 (lambda ()
                   (define-key ,mode-map
                     [remap end-of-buffer] ',fname))))))
;;
(my-special-beginning-of-buffer dired
				(while (not (ignore-errors (dired-get-filename)))
				  (dired-next-line 1)))
(my-special-end-of-buffer dired
			  (dired-previous-line 1))
;;
(my-special-beginning-of-buffer occur
				(occur-next 1))
(my-special-end-of-buffer occur
			  (occur-prev 1))
;;
(my-special-beginning-of-buffer ibuffer
				(ibuffer-forward-line 1))
(my-special-end-of-buffer ibuffer
			  (ibuffer-backward-line 1))
;;
(my-special-beginning-of-buffer recentf-dialog
				(when (re-search-forward "^  \\[" nil t)
				  (goto-char (match-beginning 0))))
(my-special-end-of-buffer recentf-dialog
			  (re-search-backward "^  \\[" nil t))
;;
(my-special-beginning-of-buffer org-agenda
				(org-agenda-next-item 1))
(my-special-end-of-buffer org-agenda
			  (org-agenda-previous-item 1))
;; --------------------------------------------------------------------------------------------------
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(LaTeX-default-author "Laurent Hoeltgen")
 '(LaTeX-default-environment "equation")
 '(LaTeX-default-options (quote ("a4paper, 12pt")))
 '(LaTeX-default-style "scrartcl")
 '(LaTeX-font-list
   (quote
    ((1 "" "" "\\mathcal{" "}")
     (2 "\\textbf{" "}" "\\mathbf{" "}")
     (3 "\\textsc{" "}")
     (5 "\\emph{" "}")
     (6 "\\textsf{" "}" "\\mathsf{" "}")
     (9 "\\textit{" "}" "\\mathit{" "}")
     (13 "\\textmd{" "}")
     (14 "\\textnormal{" "}" "\\mathnormal{" "}")
     (18 "\\textrm{" "}" "\\mathrm{" "}")
     (19 "\\textsl{" "}" "\\mathbb{" "}")
     (20 "\\texttt{" "}" "\\mathtt{" "}")
     (21 "\\textup{" "}")
     (4 "" "" t)
     (24 "\\eidx*{" "}"))))
 '(LaTeX-includegraphics-read-file (quote LaTeX-includegraphics-read-file-relative))
 '(LaTeX-indent-level 6)
 '(LaTeX-indent-level-item-continuation 8)
 '(LaTeX-item-indent -6)
 '(LaTeX-label-alist
   (quote
    (("figure" . LaTeX-figure-label)
     ("table" . LaTeX-table-label)
     ("figure*" . LaTeX-figure-label)
     ("table*" . LaTeX-table-label)
     ("equation" . LaTeX-equation-label)
     ("eqnarray" . LaTeX-eqnarray-label)
     ("theorem" . "thm:")
     ("corollary" . "thm:")
     ("proposition" . "thm:")
     ("lemma" . "thm:")
     ("definition" . "thm:"))))
 '(LaTeX-math-abbrev-prefix "#")
 '(LaTeX-math-list
   (quote
    (("C-v" "vec"
      ("private")
      nil)
     ("C-a" "abs*"
      ("private")
      nil))))
 '(LaTeX-math-menu-unicode t)
 '(LaTeX-top-caption-list (quote ("table" "algorithm")))
 '(TeX-auto-local ".auto")
 '(TeX-auto-save t)
 '(TeX-command-list
   (quote
    ((#("LatexMk" 0 1
	(idx 1))
      "latexmk %(-PDF)%S%(mode) %(file-line-error) %(extraopts) %t" TeX-run-latexmk nil
      (plain-tex-mode latex-mode doctex-mode)
      :help "Run LatexMk")
     ("TeX" "%(PDF)%(tex) %(file-line-error) %(extraopts) %`%S%(PDFout)%(mode)%' %t" TeX-run-TeX nil
      (plain-tex-mode texinfo-mode ams-tex-mode)
      :help "Run plain TeX")
     (#("LaTeX" 0 1
	(idx 2))
      "%`%l%(mode)%' %t" TeX-run-TeX nil
      (latex-mode doctex-mode)
      :help "Run LaTeX")
     ("Makeinfo" "makeinfo %(extraopts) %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with Info output")
     ("Makeinfo HTML" "makeinfo %(extraopts) --html %t" TeX-run-compile nil
      (texinfo-mode)
      :help "Run Makeinfo with HTML output")
     ("AmSTeX" "amstex %(PDFout) %(extraopts) %`%S%(mode)%' %t" TeX-run-TeX nil
      (ams-tex-mode)
      :help "Run AMSTeX")
     ("ConTeXt" "%(cntxcom) --once --texutil %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt once")
     ("ConTeXt Full" "%(cntxcom) %(extraopts) %(execopts)%t" TeX-run-TeX nil
      (context-mode)
      :help "Run ConTeXt until completion")
     (#("BibTeX" 0 1
	(idx 3))
      "bibtex %s" TeX-run-BibTeX nil t :help "Run BibTeX")
     (#("Biber" 0 1
	(idx 4))
      "biber %s" TeX-run-Biber nil t :help "Run Biber")
     (#("View" 0 1
	(idx 5))
      "%V" TeX-run-discard-or-function t t :help "Run Viewer")
     (#("Print" 0 1
	(idx 6))
      "%p" TeX-run-command t t :help "Print the file")
     (#("Queue" 0 1
	(idx 7))
      "%q" TeX-run-background nil t :help "View the printer queue" :visible TeX-queue-command)
     (#("File" 0 1
	(idx 8))
      "%(o?)dvips %d -o %f " TeX-run-dvips t t :help "Generate PostScript file")
     (#("Dvips" 0 1
	(idx 9))
      "%(o?)dvips %d -o %f " TeX-run-dvips nil t :help "Convert DVI file to PostScript")
     (#("Dvipdfmx" 0 1
	(idx 10))
      "dvipdfmx %d" TeX-run-dvipdfmx nil t :help "Convert DVI file to PDF with dvipdfmx")
     (#("Ps2pdf" 0 1
	(idx 11))
      "ps2pdf %f" TeX-run-ps2pdf nil t :help "Convert PostScript file to PDF")
     (#("Index" 0 1
	(idx 12))
      "makeindex %s" TeX-run-index nil t :help "Run makeindex to create index file")
     (#("Xindy" 0 1
	(idx 13))
      "texindy %s" TeX-run-command nil t :help "Run xindy to create index file")
     (#("Check" 0 1
	(idx 14))
      "lacheck %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for correctness")
     (#("ChkTeX" 0 1
	(idx 15))
      "chktex -v6 %s" TeX-run-compile nil
      (latex-mode)
      :help "Check LaTeX file for common mistakes")
     (#("Spell" 0 1
	(idx 16))
      "(TeX-ispell-document \"\")" TeX-run-function nil t :help "Spell-check the document")
     (#("Clean" 0 1
	(idx 17))
      "TeX-clean" TeX-run-function nil t :help "Delete generated intermediate files")
     (#("Clean All" 0 1
	(idx 18))
      "(TeX-clean t)" TeX-run-function nil t :help "Delete generated intermediate and output files")
     (#("Other" 0 1
	(idx 19))
      "" TeX-run-command t t :help "Run an arbitrary command")
     (#("Makeglossaries" 0 1
	(idx 20))
      "makeglossaries %s" TeX-run-command nil
      (latex-mode)
      :help "Make glossary"))))
 '(TeX-debug-bad-boxes t)
 '(TeX-debug-warnings t)
 '(TeX-electric-math (quote ("$" . "$")))
 '(TeX-electric-sub-and-superscript t)
 '(TeX-error-overview-open-after-TeX-run t)
 '(TeX-error-overview-setup (quote separate-frame))
 '(TeX-master nil)
 '(TeX-parse-self t)
 '(TeX-source-correlate-mode t)
 '(TeX-view-program-list
   (quote
    (("Skim"
      ("/Applications/Skim.app/Contents/SharedSupport/displayline -b -g %n %o %b")
      nil))))
 '(TeX-view-program-selection
   (quote
    ((output-dvi "open")
     (output-pdf "Skim")
     (output-html "open"))))
 '(backup-directory-alist (quote (("." . "~/.emacs.backup/"))))
 '(bibtex-completion-bibliography "~/Dropbox/Library.bib")
 '(blink-cursor-mode nil)
 '(c-basic-offset 8)
 '(c-default-style
   (quote
    ((c-mode . "stroustrup")
     (c++-mode . "stroustrup")
     (java-mode . "java")
     (awk-mode . "awk")
     (other . "gnu"))))
 '(c-echo-syntactic-information-p t)
 '(column-number-mode t)
 '(company-bibtex-bibliography "~/Dropbox/Library.bib")
 '(company-tooltip-align-annotations t)
 '(confirm-kill-emacs (quote y-or-n-p))
 '(custom-enabled-themes (quote (abyss)))
 '(custom-safe-themes
   (quote
    ("a14dd0b389ef4d6f867d3072d84935603bc88657a18e0409bbb460e7cfc991f5" "f9574c9ede3f64d57b3aa9b9cef621d54e2e503f4d75d8613cbcc4ca1c962c21" default)))
 '(delete-by-moving-to-trash t)
 '(delete-selection-mode t)
 '(dired-use-ls-dired nil)
 '(display-time-24hr-format t)
 '(display-time-day-and-date t)
 '(display-time-mode t)
 '(ebib-bib-search-dirs (quote ("~/Dropbox/")))
 '(ebib-bibtex-dialect (quote biblatex))
 '(ebib-preload-bib-files (quote ("~/Drobpox/Library.bib")))
 '(echo-keystrokes 0.1)
 '(electric-layout-mode t)
 '(electric-pair-mode t)
 '(enable-recursive-minibuffers t)
 '(f90-auto-keyword-case (quote downcase-word))
 '(f90-continuation-indent 8)
 '(f90-do-indent 4)
 '(f90-if-indent 4)
 '(f90-mode-hook
   (quote
    (f90-add-imenu-menu abbrev-mode auto-fill-mode company-mode company-statistics-mode)))
 '(f90-program-indent 4)
 '(f90-type-indent 4)
 '(fill-column 80)
 '(font-latex-match-reference-keywords (quote (("cref" "{") ("eidx" "*{"))))
 '(font-latex-match-slide-title-keywords
   (quote
    (("framesubtitle" "{")
     ("frametitle" "{")
     ("multisubtitle" "{"))))
 '(font-latex-match-type-command-keywords (quote (("linkgraphics" "[{"))))
 '(global-auto-revert-mode t)
 '(global-highlight-parentheses-mode t)
 '(global-hl-todo-mode t)
 '(global-undo-tree-mode t)
 '(global-whitespace-mode t)
 '(header-copyright-notice "Copyright 2017 Laurent Hoeltgen <hoeltgen@b-tu.de>
")
 '(header-date-format "%Y-%m-%d %T")
 '(hl-todo-activate-in-modes
   (quote
    (prog-mode f90-mode latex-mode tex-mode LaTeX-mode TeX-mode)))
 '(hl-todo-keyword-faces
   (quote
    (("TODO" . "#ff2600")
     ("DONE" . "#00bfff")
     ("NOTE" . "#d0bf8f")
     ("HACK" . "#ff9200")
     ("FIXME" . "#ff2600"))))
 '(indent-tabs-mode t)
 '(inhibit-startup-screen t)
 '(initial-buffer-choice (quote recentf-open-files))
 '(initial-scratch-message nil)
 '(ispell-program-name "aspell" t)
 '(kill-whole-line t)
 '(make-header-hook
   (quote
    (header-mode-line header-end-line header-copyright header-free-software header-description header-blank header-end-line header-creation-date header-modification-date header-blank header-end-line header-commentary header-blank header-end-line header-code header-eof)))
 '(ns-command-modifier (quote meta))
 '(ns-function-modifier (quote control))
 '(ns-right-alternate-modifier (quote none))
 '(ns-right-command-modifier (quote none))
 '(org-agenda-files (quote ("~/Dropbox/Documents/Agenda/")))
 '(org-agenda-skip-scheduled-delay-if-deadline (quote post-deadline))
 '(org-agenda-skip-scheduled-if-deadline-is-shown (quote not-today))
 '(org-agenda-skip-scheduled-if-done t)
 '(org-agenda-skip-unavailable-files t)
 '(org-agenda-sorting-strategy
   (quote
    ((agenda deadline-up priority-down)
     (todo priority-down category-keep)
     (tags priority-down category-keep)
     (search category-keep))))
 '(org-agenda-span 14)
 '(org-agenda-todo-ignore-deadlines (quote all))
 '(org-agenda-todo-ignore-scheduled (quote all))
 '(org-archive-location
   "~/Dropbox/Documents/Agenda/archive.org::datetree/* Finished Tasks")
 '(org-capture-templates
   (quote
    (("w" "A short todo reminder for work notes." checkitem
      (file+olp "~/Dropbox/Documents/Agenda/inbox.org" "Capture" "Work")
      "- [ ] %U %i %?")
     ("p" "A short todo reminder for private notes." checkitem
      (file+olp "~/Dropbox/Documents/Agenda/inbox.org" "Capture" "Private")
      "- [ ] %U %i %?"))))
 '(org-confirm-babel-evaluate nil)
 '(org-datetree-add-timestamp (quote inactive))
 '(org-deadline-warning-days 0)
 '(org-default-notes-file "~/Dropbox/Documents/Agenda/inbox.org")
 '(org-default-priority 65)
 '(org-directory "~/Dropbox/Documents/Agenda/")
 '(org-ellipsis " ⤵")
 '(org-enforce-todo-checkbox-dependencies t)
 '(org-enforce-todo-dependencies t)
 '(org-fontify-done-headline t)
 '(org-fontify-whole-heading-line t)
 '(org-log-done (quote note))
 '(org-log-into-drawer t)
 '(org-log-refile nil)
 '(org-mode-hook
   (quote
    (er/add-org-mode-expansions
     #[0 "\300\301\302\303\304$\207"
	 [add-hook change-major-mode-hook org-show-block-all append local]
	 5]
     #[0 "\300\301\302\303\304$\207"
	 [add-hook change-major-mode-hook org-babel-show-result-all append local]
	 5]
     org-babel-result-hide-spec org-babel-hide-all-hashes org-sticky-header-mode)))
 '(org-modules (quote (org-bibtex org-docview org-gnus org-info)))
 '(org-pretty-entities t)
 '(org-priority-faces
   (quote
    ((65 :foreground "OrangeRed" :weight bold)
     (66 :foreground "gold2")
     (67 :foreground "LimeGreen"))))
 '(org-ref-default-bibliography (quote ("/Users/laurent/Dropbox/Library.bib")))
 '(org-ref-pdf-directory "/Users/laurent/Dropbox/Library")
 '(org-refile-targets (quote ((org-agenda-files :maxlevel . 4))))
 '(org-refile-use-outline-path (quote full-file-path))
 '(org-startup-with-inline-images t)
 '(org-startup-with-latex-preview t)
 '(org-tag-alist
   (quote
    ((:startgroup)
     ("TODO" . 116)
     ("DONE" . 100)
     ("CANCELLED" . 99)
     (:endgroup)
     ("teaching" . 84)
     ("research" . 82)
     ("private" . 80)
     ("note" . 78))))
 '(org-tag-faces
   (quote
    (("TODO" :foreground "red" :weight bold)
     ("DONE" :foreground "forest green" :weight bold)
     ("HOLD" :foreground "orange" :weight bold)
     ("CANCELLED" :foreground "blue" :weight bold))))
 '(org-tags-column -100)
 '(org-tags-sort-function (quote string<))
 '(org-todo-keywords
   (quote
    ((sequence "TODO(t)" "|" "DONE(d!/@)")
     (sequence "HOLD(h@/!)" "|" "CANCELLED(c@/!)"))))
 '(org-todo-state-tags-triggers
   (quote
    (("CANCELLED"
      ("CANCELLED" . t))
     ("HOLD"
      ("HOLD" . t))
     (done
      ("HOLD"))
     ("TODO"
      ("CANCELLED")
      ("HOLD"))
     ("NEXT"
      ("CANCELLED")
      ("HOLD"))
     ("DONE"
      ("CANCELLED")
      ("HOLD")))))
 '(org-treat-insert-todo-heading-as-state-change t)
 '(org-use-sub-superscripts (quote {}))
 '(package-archives
   (quote
    (("gnu" . "http://elpa.gnu.org/packages/")
     ("melpa-stable" . "http://melpa-stable.milkbox.net/packages/")
     ("melpa" . "https://melpa.org/packages/")
     ("org" . "http://orgmode.org/elpa/"))))
 '(package-selected-packages
   (quote
    (org-plus-contrib org-sticky-header bibretrieve org-bookmark-heading org smart-yank org-ref ivy-hydra gscholar-bibtex auctex-latexmk company-auctex auctex latex-pretty-symbols ws-butler move-text company-math math-symbol-lists embrace auto-indent-mode expand-region fancy-narrow counsel company-bibtex undo-tree golden-ratio-scroll-screen highlight-parentheses hl-line+ highlight-indent-guides hl-todo smex visual-fill-column visual-regexp visual-regexp-steroids bibtex-utils abyss-theme all-the-icons bm ebib goto-last-change hlinum ivy-bibtex neotree volatile-highlights which-key biblio company-statistics peep-dired dired-narrow company yasnippet comment-dwim-2)))
 '(plantuml-jar-path "/Users/laurent/bin/plantuml.jar")
 '(powerline-default-separator (quote bar))
 '(projectile-indexing-method (quote native))
 '(read-buffer-completion-ignore-case t)
 '(recentf-auto-cleanup 30)
 '(recentf-exclude
   (quote
    ("\\\\.el\\\\'" "\\\\.aux\\\\'" "\\\\.log\\\\'" "^/var/folders\\\\.*" "COMMIT_EDITMSG\\\\'" ".*-autoloads\\\\.el\\\\'" "[/\\\\]\\\\.elpa/")))
 '(recentf-mode t)
 '(reftex-default-bibliography (quote ("~/Dropbox/Library.bib")))
 '(reftex-index-follow-mode t)
 '(reftex-label-alist
   (quote
    (("theorem" 65 "thm:" "~\\ref{%s}" t
      ("Theorem" "theorem")
      -3)
     ("proposition" 66 "thm:" "~\\ref{%s}" t
      ("Proposition" "proposition")
      -3)
     ("lemma" 67 "thm:" "~\\ref{%s}" t
      ("Lemma" "lemma")
      -3)
     ("definition" 68 "thm:" "~\\ref{%s}" t
      ("Definition" "definition")
      -3)
     ("remark" 69 "rem:" "~\\ref{%s}" t
      ("Remark" "remark")
      -3)
     ("corollary" 70 "thm:" "~\\ref{%s}" t
      ("Corollary" "corollary")
      -3))))
 '(reftex-plug-into-AUCTeX t)
 '(reftex-section-levels
   (quote
    (("part" . 0)
     ("chapter" . 1)
     ("section" . 2)
     ("subsection" . 3)
     ("frametitle" . 4)
     ("framesubtitle" . 5)
     ("subsubsection" . 4)
     ("paragraph" . 5)
     ("subparagraph" . 6)
     ("addchap" . -1)
     ("addsec" . -2))))
 '(reftex-toc-follow-mode t)
 '(reftex-toc-include-context t)
 '(reftex-toc-include-file-boundaries t)
 '(ring-bell-function (quote ignore))
 '(safe-local-variable-values
   (quote
    ((org-tags-column . 148)
     (org-tags-column . 116)
     (TeX-master . t))))
 '(save-interprogram-paste-before-kill t)
 '(scroll-bar-mode nil)
 '(sentence-end-double-space nil)
 '(server-mode t)
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(sp-base-key-bindings (quote sp))
 '(standard-indent 8)
 '(tab-always-indent (quote complete))
 '(tool-bar-mode nil)
 '(tooltip-mode nil)
 '(uniquify-buffer-name-style (quote post-forward-angle-brackets) nil (uniquify))
 '(whitespace-style (quote (face trailing)))
 '(ws-butler-global-mode t)
 '(yas-global-mode t nil (yasnippet))
 '(yas-snippet-dirs
   (quote
    ("~/dev/dotfiles/Templates" "~/.emacs.d/snippets" yas-installed-snippets-dir)) nil (yasnippet))
 '(yas-triggers-in-field t)
 '(yas-wrap-around-region t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :stipple nil :background nil :foreground nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 140 :width normal :foundry "nil" :family "Menlo"))))
 '(highlight-blocks-depth-1-face ((t (:background "gray15"))))
 '(highlight-blocks-depth-2-face ((t (:background "gray20"))))
 '(highlight-blocks-depth-3-face ((t (:background "gray25"))))
 '(highlight-blocks-depth-4-face ((t (:background "gray30"))))
 '(highlight-blocks-depth-5-face ((t (:background "gray35"))))
 '(highlight-blocks-depth-6-face ((t (:background "gray40"))))
 '(highlight-blocks-depth-7-face ((t (:background "gray45"))))
 '(highlight-blocks-depth-8-face ((t (:background "gray50"))))
 '(highlight-blocks-depth-9-face ((t (:background "gray55"))))
 '(hl-line ((t (:background "gray25"))))
 '(hl-todo ((t (:foreground "Red" :weight bold))))
 '(ruler-mode-comment-column ((t (:inherit ruler-mode-default :foreground "gray0"))))
 '(ruler-mode-current-column ((t (:inherit ruler-mode-default :foreground "gray0" :weight bold))))
 '(ruler-mode-default ((t (:inherit default :background "dodger blue" :foreground "gray25" :box nil))))
 '(ruler-mode-fill-column ((t (:inherit ruler-mode-default :foreground "gray0"))))
 '(ruler-mode-fringes ((t (:inherit ruler-mode-default :foreground "gray0"))))
 '(ruler-mode-goal-column ((t (:inherit ruler-mode-default :foreground "gray0")))))
;; -----------------------------------------------------------------------------
;; Hooks
;; -----------------------------------------------------------------------------
(add-hook 'after-init-hook
	  (lambda()
            (winner-mode)
            (add-hook 'ediff-after-quit-hook-internal 'winner-undo)
	    (require 'auctex-latexmk)
	    (auctex-latexmk-setup)
	    (require 'yasnippet)
	    (require 'volatile-highlights)
	    (volatile-highlights-mode t)
	    (which-key-mode)
	    (ivy-mode 1)
	    (setq ivy-use-virtual-buffers t)
	    (global-set-key "\C-s" 'swiper)
	    (global-set-key (kbd "C-c C-r") 'ivy-resume)
	    (global-set-key (kbd "M-x") 'counsel-M-x)
	    (global-set-key (kbd "C-x C-f") 'counsel-find-file)
	    (global-set-key (kbd "C-h v") 'counsel-describe-variable)
	    (global-set-key (kbd "C-h f") 'counsel-describe-function)
	    ;; counsel-pt kbd?
	    ;; counsel-yank-pop doesn't work when copying from other programs.
	    (global-set-key (kbd "M-y") 'counsel-yank-pop)
	    (define-key ivy-minibuffer-map (kbd "M-y") 'ivy-next-line)
	    (define-key ivy-minibuffer-map (kbd "C-M-y") 'ivy-previous-line)
	    (global-set-key (kbd "<C-f2>") 'bm-toggle)
	    (global-set-key (kbd "<f2>")   'bm-next)
	    (global-set-key (kbd "<S-f2>") 'bm-previous)
	    (global-set-key (kbd "<left-fringe> <mouse-5>") 'bm-next-mouse)
	    (global-set-key (kbd "<left-fringe> <mouse-4>") 'bm-previous-mouse)
	    (global-set-key (kbd "<left-fringe> <mouse-1>") 'bm-toggle-mouse)
	    (global-set-key [f8] 'neotree-toggle)
	    (define-key read-expression-map (kbd "C-r") 'counsel-expression-history)
	    (require 'bibtex-utils)
	    (autoload 'ivy-bibtex "ivy-bibtex" "" t)
	    (global-set-key [remap scroll-down-command] 'golden-ratio-scroll-screen-down)
	    (global-set-key [remap scroll-up-command] 'golden-ratio-scroll-screen-up)
	    (require 'company-bibtex)
	    (add-to-list 'company-backends 'company-bibtex)
	    (require 'expand-region)
	    (global-set-key (kbd "C-=") 'er/expand-region)
	    (fancy-narrow-mode)
	    (with-eval-after-load 'flycheck
	      (flycheck-pos-tip-mode))
	    (require 'move-text)
	    (move-text-default-bindings)
            ))

;; Optimisations to garbage collection.
;; Launch GC whenever we go out of Focus.
(add-hook 'focus-out-hook 'garbage-collect)
;; Never run GC when we are in the Minibuffer.
(defun my-minibuffer-setup-hook ()
  (setq gc-cons-threshold most-positive-fixnum))
(defun my-minibuffer-exit-hook ()
  (setq gc-cons-threshold 800000))
(add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)

(add-hook 'LaTeX-mode-hook
	  (lambda ()
            (LaTeX-math-mode)
            (turn-on-reftex)
	    (toggle-hl-line-when-idle)
	    (local-set-key (kbd "C-$") 'replace-matching-parens)
            (define-key LaTeX-mode-map (kbd "$") 'self-insert-command)
            (local-set-key [f9] 'TeX-error-overview)
            (annotate-todo)
	    (company-mode)
	    (company-auctex-init)
	    (company-statistics-mode)
	    (LaTeX-add-environments
	     '("definition" LaTeX-env-label)
	     '("theorem" LaTeX-env-label)
	     '("proposition" LaTeX-env-label)
	     '("lemma" LaTeX-env-label))
	    ))

(remove-hook 'find-file-hooks 'vc-find-file-hook)

;; Open Agenda in sole window.
(add-hook 'after-save-hook 'annotate-todo)

;; auto refresh dired when file changes
(add-hook 'dired-mode-hook 'auto-revert-mode)

(add-hook 'window-configuration-change-hook (lambda () (ruler-mode 1)))

;; -----------------------------------------------------------------------------
;; Key Bindings
;; -----------------------------------------------------------------------------

;; org
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)

(define-key global-map "\C-cr"
  (lambda () (interactive) (org-capture nil "r")))

(define-key global-map "\C-cq"
  (lambda () (interactive) (org-capture nil "q")))

;; misc
(global-set-key [remap goto-line] #'goto-line-with-feedback)
(global-set-key [remap move-beginning-of-line] #'smarter-move-beginning-of-line)
(global-set-key (kbd "C-<backspace>") (lambda () (interactive) (kill-line 0)))
(global-set-key (kbd "M-;") #'comment-dwim-2)
(global-set-key (kbd "C-x C-7") #'recentf-open-files)
(global-set-key (kbd "M-g") #'goto-line)
(global-set-key (kbd "M-p") #'goto-column)
(global-set-key (kbd "C-x C-b") #'ibuffer)

(global-set-key "\M-0" 'delete-window)
(global-set-key "\M-1" 'delete-other-windows)
(global-set-key "\M-2" 'sacha/vsplit-last-buffer)
(global-set-key "\M-3" 'sacha/hsplit-last-buffer)
(global-set-key (kbd "M-4") #'er/expand-region)

;; Font size
(define-key global-map (kbd "C-+") #'text-scale-increase)
(define-key global-map (kbd "C--") #'text-scale-decrease)
(define-key global-map (kbd "C-#") #'bury-buffer)

;; -----------------------------------------------------------------------------
;; File associations
;; -----------------------------------------------------------------------------

(add-to-list 'auto-mode-alist '("\\.gp\\'" . gnuplot-mode))
(add-to-list 'auto-mode-alist '("\\.gnuplot\\'" . gnuplot-mode))

(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))

(add-to-list 'auto-mode-alist '("\\.f\\'" . f90-mode))
(add-to-list 'auto-mode-alist '("\\.F\\'" . f90-mode))
(add-to-list 'auto-mode-alist '("\\.f90\\'" . f90-mode))
(add-to-list 'auto-mode-alist '("\\.F90\\'" . f90-mode))
(add-to-list 'auto-mode-alist '("\\.f95\\'" . f90-mode))
(add-to-list 'auto-mode-alist '("\\.F95\\'" . f90-mode))
(add-to-list 'auto-mode-alist '("\\.f03\\'" . f90-mode))
(add-to-list 'auto-mode-alist '("\\.F03\\'" . f90-mode))
(add-to-list 'auto-mode-alist '("\\.f08\\'" . f90-mode))
(add-to-list 'auto-mode-alist '("\\.F08\\'" . f90-mode))
;; Consider fypp files (fortran templates to be pre-processed) to be fortran
(add-to-list 'auto-mode-alist '("\\.fypp\\'" . f90-mode))

(add-to-list 'auto-mode-alist '("\\.tex$" . LaTeX-mode))

;; This inserts ` whenever we press ° in F90 mode. Note the ° cannot be entered
;; anymore, then.
(eval-after-load 'f90
  '(define-key f90-mode-map (kbd "°") (kbd "`")))

;; Some keys to make life easier with german keyboards on Mac.
(global-set-key (kbd "C-ö") (lambda () (interactive) (insert "[")))
(global-set-key (kbd "C-ä") (lambda () (interactive) (insert "]")))
(global-set-key (kbd "C-ü") (lambda () (interactive) (insert "\\")))

(global-set-key (kbd "M-ö") (lambda () (interactive) (insert "{")))
(global-set-key (kbd "M-ä") (lambda () (interactive) (insert "}")))
(global-set-key (kbd "M-ü") (lambda () (interactive) (insert "\\")))

(define-key global-map (kbd "C-%") 'vr/replace)
(define-key global-map (kbd "M-%") 'vr/query-replace)

;; (define-key dired-mode-map "e" 'ora-ediff-files)

;; https://github.com/nasseralkmim/.emacs.d/blob/master/config.org
;; use shift-arrows to move between windows
(windmove-default-keybindings)
(put 'narrow-to-region 'disabled nil)

;; local configuration for TeX modes
(defun my-latex-mode-setup ()
  (setq-local company-backends
              (append '((company-math-symbols-latex company-latex-commands))
                      company-backends)))

(add-hook 'TeX-mode-hook 'my-latex-mode-setup)

(setq ispell-program-name "aspell"
      ispell-dictionary "english"
      ispell-dictionary-alist
      (let ((default '("[A-Za-z]" "[^A-Za-z]" "[']" nil
                       ("-B" "-d" "english" "--dict-dir"
                        "/Library/Application Support/cocoAspell/aspell6-en-6.0-0")
                       nil iso-8859-1)))
        `((nil ,@default)
          ("english" ,@default))))

(setq bibretrieve-backends '(("mrl" . 10) ("arxiv" . 5) ("zbm" . 5)))

;; Fontify broken links in org-mode
;; http://irreal.org/blog/?p=6242
;; (org-link-set-parameters
;;  "file"
;;  :face (lambda (path) (if (file-exists-p path) 'org-link 'org-warning)))

;; Fontify done checkbox items in org-mode
;; https://fuco1.github.io/2017-05-25-Fontify-done-checkbox-items-in-org-mode.html
(font-lock-add-keywords
 'org-mode
 `(("^[ \t]*\\(?:[-+*]\\|[0-9]+[).]\\)[ \t]+\\(\\(?:\\[@\\(?:start:\\)?[0-9]+\\][ \t]*\\)?\\[\\(?:X\\|\\([0-9]+\\)/\\2\\)\\][^\n]*\n\\)" 1 'org-headline-done prepend))
 'append)
