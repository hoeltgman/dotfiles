$pdf_previewer = 'open -a skim';
$pdflatex = 'pdflatex --shell-escape --synctex=1 -file-line-error -interaction=nonstopmode -halt-on-error %O %S';

add_cus_dep('glo', 'gls', 0, 'run_makeglossaries');
add_cus_dep('acn', 'acr', 0, 'run_makeglossaries');

sub run_makeglossaries {
  if ( $silent ) {
    my ($base_name, $path) = fileparse( $_[0] );
    pushd $path;
    my $return = system "makeglossaries -q $base_name";
    popd;
    return $return;
  }
  else {
    my ($base_name, $path) = fileparse( $_[0] );
    pushd $path;
    my $return = system "makeglossaries $base_name";
    popd;
    return $return;
  };
}

push @generated_exts, 'glo', 'gls', 'glg';
push @generated_exts, 'acn', 'acr', 'alg';

$clean_ext = 'acn acr alg aux bbl bcf blg fls glg glo gls glsdefs loa toc idx ilg ist ind lof log lot out run.xml rel';
$clean_ext .= '%R.ist %R.xdy %R-blx.bib %R.synctex.gz';

