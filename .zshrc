# Created by newuser for 5.0.8

autoload -Uz compinit
autoload -U promptinit
autoload -U colors && colors
# autoload -Uz vcs_info

bindkey -e

promptinit
compinit

# zstyle ':completion:*' menu select

zstyle ":completion:*" auto-description "specify: %d"
zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ":completion:*" format "Completing %d"
zstyle ":completion:*" group-name ""
zstyle ":completion:*" menu select=2
zstyle ":completion:*:default" list-colors ${(s.:.)LS_COLORS}
zstyle ":completion:*" list-colors ""
zstyle ":completion:*" list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ":completion:*" matcher-list "" "m:{a-z}={A-Z}" "m:{a-zA-Z}={A-Za-z}" "r:|[._-]=* r:|=* l:|=*"
zstyle ":completion:*" menu select=long
zstyle ":completion:*" select-prompt %SScrolling active: current selection at %p%s
zstyle ":completion:*" verbose true

zstyle ":completion:*:*:kill:*:processes" list-colors "=(#b) #([0-9]#)*=0=01;31"
zstyle ":completion:*:kill:*" command "ps -u $USER -o pid,%cpu,tty,cputime,cmd"

# zstyle ':vcs_info:*' enable hg git

# shell options
HISTFILE=~/.zsh_history 
HISTSIZE=1000
SAVEHIST=1000

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

setopt NO_BEEP
setopt C_BASES
setopt OCTAL_ZEROES
setopt PRINT_EIGHT_BIT
setopt SH_NULLCMD
setopt AUTO_CONTINUE
setopt PATH_DIRS
setopt NO_NOMATCH
setopt EXTENDED_GLOB
disable -p '^'

setopt LIST_PACKED
setopt BASH_AUTO_LIST
setopt NO_AUTO_MENU
setopt NO_CORRECT
setopt NO_ALWAYS_LAST_PROMPT

setopt AUTO_PUSHD
setopt PUSHD_IGNORE_DUPS
setopt PUSHD_MINUS

setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY

setopt PROMPT_SUBST

setopt autocd # assume "cd" when a command is a directory
setopt histignorealldups # Substitute commands in the prompt
setopt sharehistory # Share the same history between all shells
setopt extendedglob # Extended glob syntax, eg ^ to negate, <x-y> for range, (foo|bar) etc. Backwards-incompatible with bash
setopt appendhistory autocd extendedglob
unsetopt beep nomatch

setopt completealiases
setopt HIST_IGNORE_DUPS

# function prompt_char {
#     git branch >/dev/null 2>/dev/null && echo '±' && return
#     hg root >/dev/null 2>/dev/null && echo '☿' && return
#     echo '○'
# }

PROMPT="%(?.%{$fg_no_bold[green]%}✓.%{$fg_bold[red]%}✗)%{$reset_color%} [%{$fg_bold[magenta]%}%?%{$reset_color%}] %{$fg_bold[black]%}[%*] %{$fg_bold[blue]%}%n%{$reset_color%}@%{$fg_bold[blue]%}%m %{$fg_bold[red]%}%~ %{$reset_color%}%# %F{magenta}"
#RPROMPT=$(check_cwd)
preexec () { echo -ne "\e[0m" }

# Automatically updates time in prompt, but messes with return status.
# _prompt_and_resched() { sched +1 _prompt_and_resched; zle && zle reset-prompt }
# _prompt_and_resched

##
# Environment variables
#

# basedir defaults, in case they're not already set up.
# http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html
if [[ -z "$XDG_DATA_HOME" ]]; then
	export XDG_DATA_HOME="$HOME/.local/share"
fi

if [[ -z "$XDG_CONFIG_HOME" ]]; then
	export XDG_CONFIG_HOME="$HOME/.config"
fi

if [[ -z "$XDG_CACHE_HOME" ]]; then
	export XDG_CACHE_HOME="$HOME/.cache"
fi

if [[ -z "$XDG_DATA_DIRS" ]]; then
	export XDG_DATA_DIRS="/usr/local/share:/usr/share"
fi

if [[ -z "$XDG_CONFIG_DIRS" ]]; then
	export XDG_CONFIG_DIRS="/etc/xdg"
else
	export XDG_CONFIG_DIRS="/etc/xdg:$XDG_CONFIG_DIRS"
fi

# https://gcc.gnu.org/onlinedocs/cpp/Environment-Variables.html
C_INCLUDE_PATH=''
export C_INCLUDE_PATH

CPLUS_INCLUDE_PATH=''
export CPLUS_INCLUDE_PATH

LIBRARY_PATH=''
export LIBRARY_PATH

LD_LIBRARY_PATH=''
export LD_LIBRARY_PATH

alias ls="ls --color=auto"
alias grep="grep --color=auto"
alias diff="colordiff"

# Color man pages
# https://gist.github.com/cocoalabs/2fb7dc2199b0d4bf160364b8e557eb66
man() {
	env \
		LESS_TERMCAP_mb=$(printf "\e[1;31m") \
		LESS_TERMCAP_md=$(printf "\e[1;31m") \
		LESS_TERMCAP_me=$(printf "\e[0m") \
		LESS_TERMCAP_se=$(printf "\e[0m") \
		LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
		LESS_TERMCAP_ue=$(printf "\e[0m") \
		LESS_TERMCAP_us=$(printf "\e[1;32m") \
			man "$@"
}
